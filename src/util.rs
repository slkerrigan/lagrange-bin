pub fn print_polynom(polynom: &Vec<f64>) {
    if polynom.len() == 0 {
        return;
    }


    let mut result = String::new();

    let sign = |coefficient: f64| if coefficient >= 0. { "+ " } else { "" };

    let coeff = |coefficient: f64| if coefficient == 1. {
        String::new()
    } else {
        format!("{}", coefficient)
    };

    for index in (0..polynom.len()).rev() {
        if polynom[index] == 0. {
            continue;
        }
        if index > 1 {
            if index == polynom.len() - 1 {
                result.push_str(format!("{}*x^{} ", coeff(polynom[index]).as_str(), index).as_str());
            } else {
                result.push_str(format!("{}{}*x^{} ",
                                        sign(polynom[index]),
                                        coeff(polynom[index]),
                                        index)
                    .as_str());
            }
        } else if index == 1 {
            if index == polynom.len() - 1 {
                result.push_str(format!("{}*x ", coeff(polynom[index]).as_str()).as_str());
            } else {
                result.push_str(format!("{}{}*x ",
                                        sign(polynom[index]),
                                        coeff(polynom[index]).as_str())
                    .as_str());
            }
        } else {
            if index == polynom.len() - 1 {
                result.push_str(format!("{} ", coeff(polynom[index]).as_str()).as_str());
            } else {
                result.push_str(format!("{}{}",
                                        sign(polynom[index]),
                                        coeff(polynom[index]).as_str())
                    .as_str());
            }
        }
    }

    if result.len() == 0 {
        result.push('0');
    }

    println!("{}", result.as_str());
}
