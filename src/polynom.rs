use std::iter;



pub fn sub(p1: &Vec<f64>, p2: &Vec<f64> ) -> Vec<f64> {
    return sum(p1, &mulc(p2, -1.))
}

pub fn sum(p1: &Vec<f64>, p2: &Vec<f64>) -> Vec<f64> {
    let (longer, shorter) = if p1.len() > p2.len() { (p1, p2) } else {(p2, p1)};
    longer.iter()
        .zip(shorter.iter()
            .chain(iter::repeat(&0.0)))
        .map(|(&a, &b)| a + b)
        .collect()
}

pub fn mulc(polynom: &Vec<f64>, constant: f64) -> Vec<f64> {
    polynom.iter().map(|x| x*constant).collect()
}

pub fn mul(p1: &Vec<f64>, p2: &Vec<f64>) -> Vec<f64> {
    let mut result: Vec<f64> = vec![0.; p1.len() - 1 + p2.len()];

    for i in 0..p1.len() {
        for j in 0..p2.len() {
            result[i + j] += p1[i] * p2[j];
        }
    }

    result
}
