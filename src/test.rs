#[cfg(test)]
mod tests {
    use polynom::*;
    use lagrange::*;

    #[test]
    pub fn add_test() {
        let p1 = vec![1., 2.];
        let p2 = vec![1., 2.];

        let p3 = sum(&p1, &p2);

        assert_eq!(p3, vec![2., 4.]);


        let p4 = vec![1., 2.];
        let p5 = vec![0., 2., 3.];

        let p6 = sum(&p4, &p5);

        assert_eq!(p6, vec![1., 4., 3.]);
    }

    #[test]
    fn sub_test() {
        let p1 = vec![1., 2.];
        let p2 = vec![1., 2.];

        let p3 = sub(&p1, &p2);

        assert_eq!(p3, vec![0., 0.]);
    }

    #[test]
    fn mulc_test() {
        let p1 = vec![1.0,2.0];
        let result = mulc(&p1, 5.0);
        assert_eq!(result, vec![5.0, 10.0])
    }

    #[test]
    fn mul_test() {
        let p1 = vec![1., -1.];
        let p2 = vec![1., 1.];

        let p3 = mul(&p1, &p2);

        assert_eq!(p3, vec![1., 0., -1.]);

        let p4 = vec![1., 2.];
        let p5 = vec![1., 2.];

        let p6 = mul(&p4, &p5);

        assert_eq!(p6, vec![1., 4., 4.]);


        let p7 = vec![2., -1.];
        let p8 = vec![2., 1.];

        let p9 = mul(&p7, &p8);

        assert_eq!(p9, vec![4., 0., -1.]);

        let p10 = vec![-1., 5.];
        let p11 = vec![71., 3.];

        let p12 = mul(&p10, &p11);

        assert_eq!(p12, vec![-71., 352., 15.]);

        let p13 = vec![1.];

        let p14 = vec![3., 2.];

        let p15 = mul(&p13, &p14);
        assert_eq!(p15, vec![3., 2.]);
    }


    #[test]
    fn lagrange_polynom_test() {
        let xx = vec![-1.0, 0.0, 1.0];
        let yy = vec![-1.0, 0.0, 1.0];
        let lagrange = calc_lagrange_polynom(&xx, &yy);
        assert!(lagrange.len() >= 2);
        assert_eq!(lagrange[1], 1.0);
    }
}