mod util;
mod polynom;
mod lagrange;
mod test;
//mod test;

use lagrange::*;
use util::*;

fn main() {
    let xx = vec![-3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0];
    let yy = vec![-3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0];

    /*
    let xx = vec![-1.5, -0.75, 0.0, 0.75, 1.5];
    let yy = vec![-14.1014, -0.931596, 0.0, 0.931596, 14.1014];
    */


    let lp = calc_lagrange_polynom(&xx, &yy);

    println!("polynom: {:?}", lp);

    print_polynom(&lp);
}
