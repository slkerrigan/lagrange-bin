use polynom::*;


pub fn calc_lagrange_polynom(x_values: &Vec<f64>, y_values: &Vec<f64>) -> Vec<f64> {
    assert!(x_values.len() == y_values.len());

    let mut result = vec![0.];
    for basis_index in 0..x_values.len() {
        let y = y_values[basis_index];
        let bp = calc_basis_polynom(&x_values, &y_values, basis_index);
        result = sum(&result, &mulc(&bp, y));
    }

    return result;
}

pub fn calc_basis_polynom(x_values: &Vec<f64>, y_values: &Vec<f64>, basis_index: usize) -> Vec<f64> {
    assert!(x_values.len() == y_values.len());


    let mut denominator = 1.;

    let mut numerator = vec![1.];


    let i = basis_index;

    let x_i = x_values[i];
    for j in 0..(x_values.len()) {
        /*
        j = 0..i-1, i+1..j

        (x - x_j)
        ---------
        (x_i - x_j)
        */

        if j != i {
            let x_j = x_values[j];

            denominator *= x_i - x_j;

            numerator = mul(&numerator, &vec![-x_j, 1.]);
        }


    }

    //println!("Denominator {}", denominator);


    return mulc(&numerator, 1.0 / denominator);
}
